// async function getData() {
//     const DATA = await fetch('https://ajax.test-danit.com/api/swapi/films');
//
//     try {
//         const result = await DATA.json();
//         console.log(result);
//
//         const list = document.getElementById('list');
//         list.innerHTML = '';
//
//         result.forEach((film) => {
//             const {
//                 name: title,
//                 director,
//                 episodeId,
//                 characters: charactersLinks,
//             } = film;
//             let div = document.createElement('div');
//             div.textContent = `Title: ${title}, Director: ${director}, Episode: ${episodeId} `;
//             div.style.fontSize = '50px';
//             list.appendChild(div);
//         });
//     } catch (error) {
//         console.log(error);
//     }
//
//     if (!DATA.ok) {
//         const message = 'Error: ' + res.status;
//         throw new Error(message);
//     }
//     console.log(DATA);
// }

// getData()
//     .then(() => {
//         console.log('Data fetched successfully');
//     })
//     .catch((error) => {
//         console.error('Error fetching data:', error);
//     });

async function getData() {
    const DATA = await fetch('https://ajax.test-danit.com/api/swapi/films');

    try {
        const result = await DATA.json();
        console.log('Result from API:', result);

        const list = document.getElementById('list');
        list.innerHTML = '';

        for (const film of result) {
            const {
                name: title,
                director,
                episodeId,
                characters: charactersLinks,
                openingCrawl,
            } = film;

            let div = document.createElement('div');
            div.innerHTML = `Title: ${title}, Director: ${director}, Episode: ${episodeId}. <br><br>${openingCrawl}`;
            div.style.fontSize = '25px';

            let charactersList = document.createElement('ol');
            charactersList.style.marginLeft = '20px';
            if (charactersLinks) {
                try {
                    const characterPromises = charactersLinks.map((link) =>
                        fetch(link).then((response) => response.json())
                    );
                    const characters = await Promise.all(characterPromises);

                    characters.forEach((character) => {
                        let charLi = document.createElement('li');
                        charLi.textContent = `Character: ${character.name}`;
                        charactersList.appendChild(charLi);
                    });
                } catch (characterError) {
                    console.log(
                        'Ошибка при получении персонажей:',
                        characterError
                    );
                }
            }

            div.appendChild(charactersList);
            list.appendChild(div);
        }
    } catch (error) {
        console.log(error);
    }

    if (!DATA.ok) {
        const message = 'Error: ' + DATA.status;
        throw new Error(message);
    }
    console.log(DATA);
}

getData()
    .then(() => {
        console.log('Data fetched successfully');
    })
    .catch((error) => {
        console.error('Error fetching data:', error);
    });

// Первый код я сделал изначально, но потом пришлось переделывать и делать через for()
